import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_starter_private/components/form/text_form_border.dart';
import 'package:flutter_starter_private/model/movie_list_response.dart';
import 'package:flutter_starter_private/page/example/example_page.dart';
import 'package:flutter_starter_private/page/register/register_page.dart';
import 'package:flutter_starter_private/utils/future_delayer.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:integration_test/integration_test.dart';
import 'package:mocktail/mocktail.dart';
import '../test/model/movie_list_model_test.dart' as listModel;
import '../lib/main.dart' as app;

void main() {
  IntegrationTestWidgetsFlutterBinding.ensureInitialized();

  Widget createWidgetUnderTest() {
    return MaterialApp(
      title: 'Example',
      home: ExamplePage(),
    );
  }

  testWidgets(
    "test my example age",
    (WidgetTester tester) async {
      runApp(createWidgetUnderTest());
      await tester.pump(const Duration(seconds: 4));
      await tester.pumpAndSettle();
      await tester.pump();
      expect(find.text('example'), findsNWidgets(3));
    },
  );

  testWidgets(
    "test my real app LOGIN",
    (WidgetTester tester) async {
      app.main();
      await tester.pumpAndSettle();
      // wait for splash screen
      await tester.pump(const Duration(seconds: 5));
      await tester.pumpAndSettle();
      expect(find.text('Masuk'), findsNWidgets(2));

      // tap the first favorite
      final email = find.byKey(const Key('email'));
      await tester.enterText(email, 'aulia.ramadahan@gmail.com');
      final password = find.byKey(const Key('password'));
      await tester.enterText(password, 'tes12345');
      final loginButton = find.byKey(Key('login-button'));

      // not implemented yet so nothing happened
      await tester.tap(loginButton);
      await tester.pump(Duration(seconds: 2));
      await tester.pumpAndSettle();
      // check if go back to

    },
  );

  testWidgets(
    "test my real app REGISTER",
    (WidgetTester tester) async {
      app.main();
      await tester.pumpAndSettle();
      // wait for splash screen
      await tester.pump(const Duration(seconds: 5));
      await tester.pumpAndSettle();
      expect(find.text('Belum Punya akun? Register'), findsOneWidget);

      // go to register
      final regisButton = find.byType(GestureDetector).last;
      await tester.tap(regisButton);
      await tester.pump(const Duration(seconds: 2));
      await tester.pumpAndSettle();
      expect(find.byType(RegisterPage), findsOneWidget);
      // check if go back to

      // check all textform border if can enter
      final listForm = find.byType(TextFormBorder).evaluate();
      for (var element in listForm)  {
        await tester.enterText(find.byWidget(element.widget), 'tes12345');
      }
      await tester.pumpAndSettle();
      await tester.pump(const Duration(seconds: 2));

    },
  );
}
