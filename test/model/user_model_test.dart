import 'package:flutter_starter_private/model/error_response.dart';
import 'package:flutter_starter_private/model/user_data.dart';
import 'package:flutter_starter_private/utils/converter.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test(
    "check json transform for User Data and response error",
    () {
      final dataFromJson = jsonToList(jsonUserList, UserData.fromJson);
      expect(dataFromJson[0].toJson(), jsonUserList[0]);
      final errorResp = ErrorResponse.fromJson({"field": "gender", "message": "can't be blank, can be male of female"});
      expect(errorResp.field, "gender");
    },
  );
}

const jsonUserList = [
  {"id": 2364, "name": "Chandani Khatri", "email": "chandani_khatri@brekke.co", "gender": "female", "status": "inactive"},
  {"id": 2299, "name": "Msgr. Sushma Arora", "email": "arora_sushma_msgr@smith.io", "gender": "female", "status": "inactive"},
  {"id": 2296, "name": "Devadatt Varma", "email": "devadatt_varma@leuschke-flatley.co", "gender": "male", "status": "active"},
  {"id": 2190, "name": "Arun Nehru", "email": "arun_nehru@hickle-hansen.com", "gender": "female", "status": "inactive"},
  {"id": 2187, "name": "Inder Sinha", "email": "inder_sinha@ullrich.name", "gender": "male", "status": "inactive"},
  {"id": 2186, "name": "Dipankar Naik CPA", "email": "dipankar_naik_cpa@oreilly.org", "gender": "male", "status": "inactive"},
  {"id": 2164, "name": "Brajesh Varman", "email": "brajesh_varman@frami.biz", "gender": "male", "status": "inactive"},
  {"id": 2163, "name": "Dinesh Kocchar", "email": "kocchar_dinesh@jacobi.info", "gender": "female", "status": "active"},
  {"id": 2161, "name": "Bhuvaneshwar Singh", "email": "singh_bhuvaneshwar@koch.name", "gender": "female", "status": "inactive"},
  {"id": 2160, "name": "Ramesh Panicker", "email": "panicker_ramesh@bartoletti.name", "gender": "female", "status": "inactive"}
];
