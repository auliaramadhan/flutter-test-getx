import 'package:flutter_starter_private/model/movie_detail_model.dart';
import 'package:flutter_starter_private/model/movie_list_response.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test(
    "check json transform for ListMovieResponse ",
    () {
      final dataFromJson = ListMovieResponse.fromJson(_mapData);
      final revertData = dataFromJson.toJson();
      for (var keyName in revertData.keys) {
        if (keyName == 'results') {
          expect(revertData[keyName]!.length, _mapData[keyName]!.length);
          continue;
        }
        expect(revertData[keyName], _mapData[keyName]);
      }
    },
  );
}

const Map<String, dynamic> _mapData = {
  "page": 1,
  "results": [
    {
      "adult": false,
      "backdrop_path": "/bQXAqRx2Fgc46uCVWgoPz5L5Dtr.jpg",
      "genre_ids": [28, 14, 878],
      "id": 436270,
      "original_language": "en",
      "original_title": "Black Adam",
      "overview":
          "Nearly 5,000 years after he was bestowed with the almighty powers of the Egyptian gods—and imprisoned just as quickly—Black Adam is freed from his earthly tomb, ready to unleash his unique form of justice on the modern world.",
      "popularity": 4328.431,
      "poster_path": "/3zXceNTtyj5FLjwQXuPvLYK5YYL.jpg",
      "release_date": "2022-10-19",
      "title": "Black Adam",
      "video": false,
      "vote_average": 6.9,
      "vote_count": 1058
    },
    {
      "adult": false,
      "backdrop_path": "/y5Z0WesTjvn59jP6yo459eUsbli.jpg",
      "genre_ids": [27, 53],
      "id": 663712,
      "original_language": "en",
      "original_title": "Terrifier 2",
      "overview":
          "After being resurrected by a sinister entity, Art the Clown returns to Miles County where he must hunt down and destroy a teenage girl and her younger brother on Halloween night.  As the body count rises, the siblings fight to stay alive while uncovering the true nature of Art's evil intent.",
      "popularity": 4528.571,
      "poster_path": "/b6IRp6Pl2Fsq37r9jFhGoLtaqHm.jpg",
      "release_date": "2022-10-06",
      "title": "Terrifier 2",
      "video": false,
      "vote_average": 7,
      "vote_count": 579
    },
    {
      "adult": false,
      "backdrop_path": "/8sMmAmN2x7mBiNKEX2o0aOTozEB.jpg",
      "genre_ids": [28, 12, 878],
      "id": 505642,
      "original_language": "en",
      "original_title": "Black Panther: Wakanda Forever",
      "overview":
          "Queen Ramonda, Shuri, M’Baku, Okoye and the Dora Milaje fight to protect their nation from intervening world powers in the wake of King T’Challa’s death. As the Wakandans strive to embrace their next chapter, the heroes must band together with the help of War Dog Nakia and Everett Ross and forge a new path for the kingdom of Wakanda.",
      "popularity": 4594.775,
      "poster_path": "/sv1xJUazXeYqALzczSZ3O6nkH75.jpg",
      "release_date": "2022-11-09",
      "title": "Black Panther: Wakanda Forever",
      "video": false,
      "vote_average": 7.6,
      "vote_count": 465
    },
    {
      "adult": false,
      "backdrop_path": "/b2FxWOxe9K7ZZ1uaPOze2RJ1ajq.jpg",
      "genre_ids": [27, 28, 35],
      "id": 675054,
      "original_language": "es",
      "original_title": "MexZombies",
      "overview": "A group of teenagers must face a zombie apocalypse, and help reestablish order.",
      "popularity": 3604.019,
      "poster_path": "/85zufUxhD97k2s5Bu2u101Qd8Sg.jpg",
      "release_date": "2022-10-26",
      "title": "MexZombies",
      "video": false,
      "vote_average": 7.3,
      "vote_count": 66
    },
    {
      "adult": false,
      "backdrop_path": "/3Ykwi0ObNpqy5gAwhddmxKWs1dp.jpg",
      "genre_ids": [53, 18],
      "id": 848526,
      "original_language": "fr",
      "original_title": "Arsenault & Fils",
      "overview": "Follows the Arsenaults, a small-time poach family in Témiscouata, Quebec.",
      "popularity": 1344.132,
      "poster_path": "/2symxAMYXpJ9mJ8YOc2b5ZEKIAQ.jpg",
      "release_date": "2022-06-17",
      "title": "Family Game",
      "video": false,
      "vote_average": 5.1,
      "vote_count": 5
    }
  ],
  "total_pages": 35876,
  "total_results": 717511
};
