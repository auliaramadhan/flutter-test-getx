import 'dart:math';
import 'package:flutter_starter_private/model/user_data.dart';
import 'package:flutter_starter_private/repository/user_repository.dart';
import 'package:flutter_starter_private/utils/converter.dart';
import 'package:mocktail/mocktail.dart';
import 'package:flutter_test/flutter_test.dart';

import 'model/user_model_test.dart';

class MockUserRepository extends Mock implements UserRepository {}

void main() {
  // ini bukan final karena setup akan jalan terus di setiap tes
  late MockUserRepository mockRepo;
  final userRequest = UserData.fromJson(jsonUserList[0]);
  setUpAll(() {
    /// setup all cuma jalan sekali jadi bisa dipakai di final
    /// lebih bagus dipakai di setiap group test
    mockRepo = MockUserRepository();
  });

  group(
    "check all example",
    () {
      setUp(
        () {},
      );
      test(
        "check list user",
        () async {
          when(() => mockRepo.fetchUserList(1)).thenAnswer((_) async => jsonToList(jsonUserList, UserData.fromJson));
          final randomNumber = await mockRepo.fetchUserList(1);
          verify(() => mockRepo.fetchUserList(1)).called(1);
          expect(randomNumber.length, lessThanOrEqualTo(10));
        },
      );

      test(
        "check post",
        () async {
          when(() => mockRepo.postUser(userRequest)).thenAnswer((_) async => userRequest );
          final example = await mockRepo.postUser(userRequest);
          verify(() => mockRepo.postUser(userRequest)).called(1);
          expect(example.email, userRequest.email);
          expect(example, const TypeMatcher<UserData>());
        },
      );

      test(
        "check delete",
        () async {
          // final newRequest = 
          when(() => mockRepo.deleteUser(userRequest.id!)).thenAnswer((_) async => true );
          final example = await mockRepo.deleteUser(userRequest.id!);
          verify(() => mockRepo.deleteUser(userRequest.id!)).called(1);
          expect(example, true);
        },
      );
    },
  );
}
