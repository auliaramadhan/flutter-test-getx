import 'package:dio/dio.dart';

import '../model/user_data.dart';
import '../utils/api.dart';
import '../utils/api_url.dart';
import '../utils/converter.dart';
import '../utils/extensions.dart';

abstract class UserRepository {
  Future<List<UserData>> fetchUserList(int page, [String? query]);
  Future<UserData> postUser(UserData user);
  Future<UserData> putUser(UserData user);
  Future<bool> deleteUser(int id);
}

class UserRepositoryImpl implements UserRepository {
  final httpClient = HttpClient();

  @override
  fetchUserList(page, [query]) async {
    final rawResponse = await httpClient.get(ApiUrl.USER, query: {
      "page": page,
      "per_page": 10,
      "name": query,
    });
    if (rawResponse is DioError) {
      throw rawResponse;
    }
    // else if (rawResponse is List){
    //   if (rawResponse[0]['message'] != null) {
    //     throw("No Data");
    //   }
    // }
    return jsonToList(rawResponse, UserData.fromJson);
  }

  @override
  postUser(user) async {
    final id = user.id;
    final rawResponse = await httpClient.post('${ApiUrl.USER}/', user.toJson());
    if (rawResponse is DioError) {
      throw rawResponse;
    } else if (rawResponse is List) {
      if ((rawResponse[0] ?? {})['message'] != null) {
        throw (Exception(rawResponse[0]['message']));
      }
    }
    return UserData.fromJson(rawResponse);
  }

  @override
  putUser(user) async {
    final id = user.id;
    final rawResponse = await httpClient.put('${ApiUrl.USER}/$id', user.toJson());
    if (rawResponse is DioError) {
      throw rawResponse;
    } else if (rawResponse is List) {
      if ((rawResponse[0] ?? {})['message'] != null) {
        throw (Exception(rawResponse[0]['message']));
      }
    }
    return UserData.fromJson(rawResponse);
  }

  @override
  deleteUser(id) async {
    final rawResponse = await httpClient.delete('${ApiUrl.USER}/$id', null);
    if (rawResponse is DioError) {
      throw rawResponse;
    } else if (rawResponse is Map && rawResponse['message'] != null) {
      throw (Exception(rawResponse[0]['message']));
    }
    return true;
  }
}
