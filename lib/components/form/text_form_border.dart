import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_starter_private/theme/apptheme.dart';
import 'package:flutter_starter_private/utils/extensions.dart';
import '../../theme/app_color.dart';
import '../../theme/appfont.dart';

class TextFormBorder extends StatefulWidget {
  final String? label;
  final FormFieldValidator<String>? validator;
  final ValueChanged<String>? onChanged;
  final String hintText;
  final TextEditingController? controller;

  final TextInputType keyboardType;

  final Widget? suffixIcon;
  final BoxConstraints? suffixIconConstraints;
  final Widget? prefixIcon;
  final BoxConstraints? prefixIconConstraints;
  final Widget? suffix;
  final Widget? prefix;
  final bool enabled;
  final GestureTapCallback? onTap;

  final bool obscureText;
  final bool showAlert;

  final List<TextInputFormatter>? inputFormatters;

  final int? minLines;
  final int? maxLines;
  
  final bool readOnly;

  const TextFormBorder({
    Key? key,
    required this.label,
    this.validator,
    required this.hintText,
    this.controller,
    this.onChanged,
    this.keyboardType = TextInputType.text,
    this.suffixIcon,
    this.suffixIconConstraints,
    this.prefixIcon,
    this.prefixIconConstraints,
    this.suffix,
    this.prefix,
    this.enabled = true,
    this.readOnly = false,
    this.obscureText = false,
    this.minLines,
    this.onTap,
    this.maxLines = 1,
    this.showAlert = true,
    this.inputFormatters,
  }) : super(key: key);

  @override
  State<TextFormBorder> createState() => _TextFormBorderState();
}

class _TextFormBorderState extends State<TextFormBorder> {
  String text = '';
  

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (widget.label.isNotNull)Padding(
          padding: const EdgeInsets.only(bottom: 8.0),
          child: Text(widget.label!, style: AppFont.inputLabel),
        ),
        TextFormField(
          keyboardType: widget.keyboardType,
          onSaved: (newValue) => text = newValue!,
          onChanged: (value) {
            setState(() {
              text = value;
            });
            widget.onChanged?.call(value);
          },
          autovalidateMode: AutovalidateMode.onUserInteraction,
          enabled: widget.enabled,
          readOnly: widget.readOnly,
          controller: widget.controller,
          validator: widget.validator,
          minLines: widget.minLines,
          maxLines: widget.maxLines,
          obscureText: widget.obscureText,
          onTap: widget.onTap,
          inputFormatters: widget.inputFormatters,
          decoration: InputDecoration(
            // contentPadding: const EdgeInsets.fromLTRB(8, 16, 12, 16),
            hintText: widget.hintText,
            hintStyle: AppFont.formHint,
            // isCollapsed: true,
            // focusColor: AppColor.primary,
            // hoverColor: AppColor.primary,
            iconColor: AppColor.primary,
            fillColor: AppColor.bgDisableForm,
            filled: !widget.enabled,
            isDense: true,
            // errorText: "",
            errorStyle: const TextStyle(height: 0.5, textBaseline: TextBaseline.alphabetic),
            floatingLabelBehavior: FloatingLabelBehavior.always,
            border: AppTheme.inputBorder,
            focusedBorder: AppTheme.inputBorder,
            suffix: widget.suffix,
            suffixIcon: widget.suffixIcon,
            suffixIconConstraints: widget.suffixIconConstraints,
            prefix: widget.prefix,
            prefixIcon: widget.prefixIcon,
            prefixIconConstraints: widget.prefixIconConstraints,
            // : widget. ,
          ),
        ),
        // if (_isError && widget.showAlert )
        //   Text(
        //     'ⓘ ${widget.validator!.call(text) ?? 'error'}',
        //     style: AppFont.formlabelError,
        //   ),
        // if (_isSuccess && widget.showAlert)
        //   const Text(
        //     '✔ success',
        //     style: AppFont.formlabelSuccess,
        //   ),
      ],
    );
  }
}
