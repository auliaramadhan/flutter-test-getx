import 'package:flutter/material.dart';
import 'package:flutter_starter_private/components/form/dropdown_form_border.dart';
import 'package:flutter_starter_private/components/form/text_form_border.dart';
import 'package:flutter_starter_private/components/spacing.dart';
import 'package:flutter_starter_private/helper/utils.dart';
import 'package:flutter_starter_private/model/user_data.dart';
import 'package:flutter_starter_private/theme/appfont.dart';
import 'package:get/get.dart';
import '../../utils/extensions.dart';

class FormUserView extends StatefulWidget {
  final UserData? user;
  const FormUserView({super.key, this.user});

  @override
  State<FormUserView> createState() => FormUserViewState();
}

class FormUserViewState extends State<FormUserView> {
  final _nameCtrl = TextEditingController();
  final _emailCtrl = TextEditingController();

  UserGender? _gender;
  UserStatus? _status;

  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    if (widget.user.isNotNull) {
      _nameCtrl.text = widget.user!.name;
      _emailCtrl.text = widget.user!.email;
      _gender = widget.user!.gender;
      _status = widget.user!.status;
    }
    super.initState();
  }

  @override
  void dispose() {
    _nameCtrl.dispose();
    _emailCtrl.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TextFormBorder(
            controller: _nameCtrl,
            label: 'name',
            hintText: 'tes',
            validator: Utils.formValidator('Nama harus Diisi'),
          ),
          TextFormBorder(
            controller: _emailCtrl,
            label: 'email',
            hintText: 'tes.email@yahoo.com',
            validator: Utils.formValidator(
              'email harus diisi',
              (value) {
                if (!(value?.isEmail ?? false)) {
                // if (!RegExp(r'^\w[\w\d]*\.[\w\d]*@[\w\d]*\.[\w\d\.]{2,5}$').hasMatch(value!)) {
                  return 'email harus sesuai';
                }
              },
            ),
          ),
          DropdownFormBorder<UserGender>(
            label: 'gender',
            hintText: 'Laki-Laki',
            list: UserGender.values,
            value: _gender,
            validator: Utils.formValidator('Gender Haus diisi'),
            onChange: (value) {
              _gender = value;
            },
            textSetter: (value) => value.name,
          ),
          DropdownFormBorder<UserStatus>(
            label: 'status',
            hintText: 'Active',
            list: UserStatus.values,
            value: _status,
            validator: Utils.formValidator('Status Harus diisi'),
            onChange: (value) {
              _status = value;
            },
            textSetter: (value) => value.name,
          ),
        ],
      ),
    );
  }

  UserData? getData() {
    if (!_formKey.currentState!.validate()) {
      return null;
    }
    return UserData(
      id: widget.user?.id,
      name: _nameCtrl.text,
      email: _emailCtrl.text,
      gender: _gender!,
      status: _status!,
    );
  }
}
