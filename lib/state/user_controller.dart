import 'package:flutter_starter_private/model/user_data.dart';
import 'package:flutter_starter_private/repository/user_repository.dart';
import 'package:get/get.dart';

class UserController extends GetxController {
  static UserController to = Get.find<UserController>();

  final _repo = Get.find<UserRepository>();
  var listUser = List<UserData>.empty().obs;
  int currentPage = 1;
  var listUserSeacrh = List<UserData>.empty().obs;
  int currentPageSearch = 1;

  var isMax = false.obs;
  var isMaxSeacrh = false.obs;

  var isLoading = false.obs;

  var message = Rx<String?>(null);

  @override
  onInit() {
    super.onInit();
    ever(
      isLoading,
      (callback) {
        if (isLoading.value) {
          message.value = null;
        }
      },
    );
  }

  Future<void> fetchList({reset = false}) async {
    if (isMax.value) return;
    isLoading.value = true;
    if (reset) {
      listUser.clear();
      currentPage = 1;
      isMax.value = false;
    }
    try {
      final list = await _repo.fetchUserList(currentPage);
      if (list.length < 10) {
        isMax.value = true;
      }
      listUser.addAll(list);
      currentPage++;
    } catch (e) {
      message.value = '$e';
    } finally {
      isLoading.value = false;
    }
  }

  Future<void> fetchListSearch(String search, {bool reset = false}) async {
    if (reset) {
      listUserSeacrh.clear();
      currentPageSearch = 1;
      isMaxSeacrh.value = false;
    }
    if (isMaxSeacrh.value || search.isEmpty) return;
    isLoading.value = true;
    try {
      final list = await _repo.fetchUserList(currentPageSearch, search);
      if (list.length < 10) {
        isMaxSeacrh.value = true;
      }
      listUserSeacrh.addAll(list);
      currentPageSearch++;
    } catch (e) {
      message.value = '$e';
    } finally {
      isLoading.value = false;
    }
  }

  void resetSearch() {
    listUserSeacrh.clear();
    currentPageSearch = 1;
    isMaxSeacrh.value = false;
  }

  Future<bool> createUser(UserData user) async {
    isLoading.value = true;
    var success = false;
    try {
      final newUser = await _repo.postUser(user);
      listUser.insert(0,newUser);
      message.value = 'Create user id ${newUser.id} success';
      success = true;
    } catch (e) {
      message.value = '$e';
    } finally {
      isLoading.value = false;
    }
    return success;
  }

  Future<void> updateUser(UserData user) async {
    isLoading.value = true;
    try {
      final updatedUser = await _repo.putUser(user);
      final index = listUser.indexWhere((element) => element.id == updatedUser.id);
      if (index == -1) {
        throw ('Index NOt Found');
      }
      listUser[index] = updatedUser;
      message.value = 'update success';
    } catch (e) {
      message.value = '$e';
    } finally {
      isLoading.value = false;
    }
  }

  Future<bool> deleteUser(int id) async {
    isLoading.value = true;
    bool deletedUser = false;
    try {
      deletedUser = await _repo.deleteUser(id);
      if (!deletedUser) {
        throw ('Delete Unsuccessful');
      }
      final index = listUser.indexWhere((element) => element.id == id);
      listUser.removeAt(index);
    } catch (e) {
      message.value = '$e';
    } finally {
      isLoading.value = false;
    }
    return deletedUser;
  }
}
