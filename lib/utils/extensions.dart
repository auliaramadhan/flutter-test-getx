extension CheckNull<T> on T?  {
  bool get isNotNull => this != null ;
  bool get isNull => this == null;
}