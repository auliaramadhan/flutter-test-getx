// To parse this JSON data, do
//
//     final exampleData = exampleDataFromJson(jsonString);

import 'dart:convert';

class UserData {
  UserData({
    this.id,
    required this.name,
    required this.email,
    required this.gender,
    required this.status,
  });

  final int? id;
  final String name;
  final String email;
  final UserGender gender;
  final UserStatus status;

  UserData copyWith({
    int? id,
    String? name,
    String? email,
    UserGender? gender,
    UserStatus? status,
  }) =>
      UserData(
        id: id ?? this.id,
        name: name ?? this.name,
        email: email ?? this.email,
        gender: gender ?? this.gender,
        status: status ?? this.status,
      );

  factory UserData.fromRawJson(String str) => UserData.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory UserData.fromJson(Map<String, dynamic> json) => UserData(
        id: json["id"],
        name: json["name"],
        email: json["email"],
        gender: userGenderValues.map[json["gender"]]!,
        status: userStatusValues.map[json["status"]] ?? UserStatus.inactive,
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "email": email,
        "gender": userGenderValues.reverse[gender],
        "status": userStatusValues.reverse[status],
      };
}

enum UserGender { male, female }

final userGenderValues = EnumValues({"female": UserGender.female, "male": UserGender.male});

enum UserStatus { active, inactive }

final userStatusValues = EnumValues({"active": UserStatus.active, "inactive": UserStatus.inactive});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map) : reverseMap = map.map((k, v) => MapEntry(v, k));

  Map<T, String> get reverse {
    return reverseMap;
  }
}
