// To parse this JSON data, do
//
//     final exampleData = exampleDataFromJson(jsonString);

import 'dart:convert';

class ErrorResponse {
    ErrorResponse({
        this.field,
        this.message,
    });

    final String? field;
    final String? message;

    ErrorResponse copyWith({
        String? field,
        String? message,
    }) => 
        ErrorResponse(
            field: field ?? this.field,
            message: message ?? this.message,
        );

    factory ErrorResponse.fromRawJson(String str) => ErrorResponse.fromJson(json.decode(str));

    String toRawJson() => json.encode(toJson());

    factory ErrorResponse.fromJson(Map<String, dynamic> json) => ErrorResponse(
        field: json["field"],
        message: json["message"],
    );

    Map<String, dynamic> toJson() => {
        "field": field,
        "message": message,
    };
}
