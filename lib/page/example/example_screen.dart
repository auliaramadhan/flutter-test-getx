import 'package:flutter/material.dart';

import '../../components/button/button_primary.dart';
import '../../components/form/dropdown_form_border.dart';
import '../../components/form/text_form_border.dart';
import '../../components/spacing.dart';
import '../../theme/appfont.dart';

class ExampleScreen extends StatefulWidget {
  const ExampleScreen({Key? key}) : super(key: key);

  @override
  State<ExampleScreen> createState() => _ExampleScreenState();
}

class _ExampleScreenState extends State<ExampleScreen> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const ExtraHeight(),
        Text('example', style: AppFont.body),
        TextFormBorder(label: 'name', hintText: 'tes'),
        DropdownFormBorder<String>(
          label: 'gender',
          hintText: 'Laki-Laki',
          list: ['tes1', 'tes2'],
          textSetter: (value) => value,
        ),
        const ExtraHeight(),
        ButtonPrimary(
          text: 'tes',
          onPressed: () {},
        ),
      ],
    );
  }
}
