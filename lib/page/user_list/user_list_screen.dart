import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_starter_private/helper/debouncer.dart';
import 'package:flutter_starter_private/page/user_create/user_create_page.dart';
import 'package:flutter_starter_private/page/user_edit/user_edit_page.dart';
import 'package:flutter_starter_private/state/user_controller.dart';
import 'package:flutter_starter_private/theme/apptheme.dart';
import 'package:get/get.dart';

import '../../components/button/button_primary.dart';
import '../../components/form/dropdown_form_border.dart';
import '../../components/form/text_form_border.dart';
import '../../components/spacing.dart';
import '../../theme/appfont.dart';

class UserListScreen extends StatefulWidget {
  const UserListScreen({Key? key}) : super(key: key);

  @override
  State<UserListScreen> createState() => _UserListScreenState();
}

class _UserListScreenState extends State<UserListScreen> {
  final _searchCtrl = TextEditingController();
  final _debouncer = Debouncer(milliseconds: 1000);
  final _controller = Get.put(UserController())..fetchList();

  final _scrollCtrl = ScrollController();

  bool isSearch = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _scrollCtrl.addListener(_scrollListener);
  }

  @override
  void dispose() {
    _searchCtrl.dispose();
    _scrollCtrl.dispose();
    super.dispose();
  }

  Widget row(String name, String value) => Padding(
        padding: const EdgeInsets.symmetric(vertical: 4.0),
        child: Row(
          children: [
            SizedBox(child: Text(name, style: AppFont.bodyBold), width: 60),
            Expanded(
                flex: 1,
                child: Text(
                  ': $value',
                  style: AppFont.body,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 1,
                )),
          ],
        ),
      );

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        TextFormBorder(
          label: null,
          hintText: "Search",
          controller: _searchCtrl,
          onChanged: (value) {
            _debouncer.run(() {
              if (value.isNotEmpty) {
                setState(() {
                  isSearch = true;
                });
                UserController.to.fetchListSearch(value, reset: true);
              } else {
                UserController.to.resetSearch();
                setState(() {
                  isSearch = false;
                });
              }
            });
          },
        ),
        const ExtraHeight(),
        Obx(() {
          final list = isSearch ? _controller.listUserSeacrh : _controller.listUser;
          final isLoading = _controller.isLoading.value;
          return Expanded(
            child: Stack(
              children: [
                ListView.builder(
                  itemBuilder: (context, index) {
                    final user = list[index];
                    return GestureDetector(
                      onTap: () {
                        Get.to(() => UserEditPage(user: user));
                      },
                      child: Card(
                        shape: AppTheme.shapeRound16,
                        elevation: 5,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 4),
                          child: Column(
                            children: [
                              row('ID', '${user.id}'),
                              row('Name', user.name),
                              row('Email', user.email),
                              row('Gender', user.gender.name),
                              row('Status', user.status.name),
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                  itemCount: list.length,
                  controller: _scrollCtrl,
                ),
                if (isLoading) const Center(child: CircularProgressIndicator.adaptive()),
              ],
            ),
          );
        }),
        const ExtraHeight(),
      ],
    );
  }

  _scrollListener() {
    print(_scrollCtrl.position.extentAfter.toString());
    if (_scrollCtrl.position.extentAfter <= 0) {
      print('fetch list');
      final isLoading = _controller.isLoading.value;
      final isMax = isSearch ? _controller.isMaxSeacrh.value : _controller.isMax.value;
      if (!isLoading && !isMax) {
        if (isSearch) {
          UserController.to.fetchListSearch(_searchCtrl.text);
        } else {
          UserController.to.fetchList();
        }
      }
      // run something when reach bottom
    }
  }
}
