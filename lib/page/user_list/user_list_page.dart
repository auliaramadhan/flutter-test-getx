import 'package:flutter/material.dart';
import 'package:flutter_starter_private/page/user_create/user_create_page.dart';
import 'package:flutter_starter_private/theme/app_color.dart';
import 'package:get/get.dart';

import '../../theme/apptheme.dart';
import 'user_list_screen.dart';

class UserListPage extends StatefulWidget {
  const UserListPage({Key? key}) : super(key: key);

  @override
  State<UserListPage> createState() => _UserListPageState();
}

class _UserListPageState extends State<UserListPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text('User List'),
      ),
      floatingActionButton: CircleAvatar(
        backgroundColor: AppColor.primary,
        child: IconButton(
          onPressed: () {
            Get.to(() => const UserCreatePage());
          },
          icon: const Icon(Icons.add, color: Colors.white),
        ),
      ),
      body: SafeArea(
        child: Container(
          padding: const EdgeInsets.all(16.0),
          child: const UserListScreen(),
        ),
      ),
    );
  }
}
