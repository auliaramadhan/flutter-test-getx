import 'package:flutter/material.dart';
import 'package:flutter_starter_private/model/user_data.dart';

import '../../theme/apptheme.dart';
import 'user_edit_screen.dart';

class UserEditPage extends StatelessWidget {
  final UserData user;
  const UserEditPage({Key? key, required this.user}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text('User ${user.name}'),
      ),
      body: SafeArea(
        child: Container(
          padding: const EdgeInsets.all(16.0),
          child: UserEditScreen(user: user),
        ),
      ),
    );
  }
}
