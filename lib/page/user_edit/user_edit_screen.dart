import 'dart:async';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_starter_private/components/button/button_secondary.dart';
import 'package:flutter_starter_private/helper/utils.dart';
import 'package:flutter_starter_private/state/user_controller.dart';
import 'package:flutter_starter_private/theme/app_color.dart';
import 'package:get/get.dart';

import '../../utils/extensions.dart';
import '../../components/button/button_primary.dart';
import '../../components/spacing.dart';
import '../../components/view/form_user.dart';
import '../../model/user_data.dart';

class UserEditScreen extends StatefulWidget {
  final UserData user;
  const UserEditScreen({Key? key, required this.user}) : super(key: key);

  @override
  State<UserEditScreen> createState() => _UserEditScreenState();
}

class _UserEditScreenState extends State<UserEditScreen> {
  final _formKey = GlobalKey<FormUserViewState>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    ever(
      UserController.to.message,
      (String? p0) {
        if (p0.isNull) return;
        Utils.showAlert(text: p0 ?? '');
      },
    );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Obx(() {
          final isLoading = UserController.to.isLoading.value;
          return Stack(
            alignment: Alignment.center,
            fit: StackFit.loose,
            children: [
              FormUserView(key: _formKey, user: widget.user),
              if (isLoading)
                Container(
                  height: 300,
                  color: Colors.transparent,
                  child: const Center(child: CircularProgressIndicator.adaptive()),
                ),
            ],
          );
        }),
        const ExtraHeight(),
        ButtonPrimary(
          text: 'Simpan',
          onPressed: () {
            final data = _formKey.currentState!.getData();
            if (data != null) {
              UserController.to.updateUser(data);
            }
          },
        ),
        const ExtraHeight(),
        ButtonSecondary(
          text: 'Hapus Pengguna',
          onPressed: () async {
            final success = await UserController.to.deleteUser(widget.user.id!);
            if (success) {
              Get.back();
            }
          },
        ),
      ],
    );
  }
}
