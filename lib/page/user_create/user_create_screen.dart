import 'package:flutter/material.dart';
import 'package:flutter_starter_private/helper/utils.dart';
import 'package:flutter_starter_private/state/user_controller.dart';
import 'package:get/get.dart';

import '../../utils/extensions.dart';
import '../../components/button/button_primary.dart';
import '../../components/spacing.dart';
import '../../components/view/form_user.dart';

class UserCreateScreen extends StatefulWidget {
  const UserCreateScreen({Key? key}) : super(key: key);

  @override
  State<UserCreateScreen> createState() => _UserCreateScreenState();
}

class _UserCreateScreenState extends State<UserCreateScreen> {
  final _formKey = GlobalKey<FormUserViewState>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    ever(
      UserController.to.message,
      (String? p0) {
        if (p0.isNull) return;
        Utils.showAlert(text: p0 ?? '');
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const ExtraHeight(),
        Obx(() {
          final isLoading = UserController.to.isLoading.value;
          return Stack(
            alignment: Alignment.center,
            fit: StackFit.loose,
            children: [
              FormUserView(key: _formKey),
              if (isLoading)
                Container(
                  height: 300,
                  color: Colors.transparent,
                  child: const Center(child: CircularProgressIndicator.adaptive()),
                ),
            ],
          );
        }),
        const ExtraHeight(),
        ButtonPrimary(
          text: 'Buat Pengguna',
          onPressed: () async {
            final data = _formKey.currentState!.getData();
            if (data != null) {
              final success = await UserController.to.createUser(data);
              if (success) Get.back();
            }
          },
        ),
      ],
    );
  }
}
